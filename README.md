# MPET -- Multiphase Porous Electrode Theory

MPET development has moved to Github and is now hosted here: [https://github.com/TRI-AMDD/mpet](https://github.com/TRI-AMDD/mpet)

This repository will not be updated in the future.